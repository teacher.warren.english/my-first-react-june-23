const Auth = Component => props => {
    
    let isLoggedIn = true

    if (isLoggedIn)
        return <Component {...props} />
    else
        return <p>Warning! You are not logged in!</p>
    
}

export default Auth