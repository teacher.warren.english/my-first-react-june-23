import { NavLink } from "react-router-dom"

function Navbar() {
return (
    <>
        <NavLink to='/'>Home Page</NavLink>
        <NavLink to='/user'>User Page</NavLink>

        <hr />
    </>
)
}

export default Navbar