import { useContext } from "react"
import { UserContext } from "../Context/UserContext"
import Navbar from "./Navbar"

function User() {

    const [user,] = useContext(UserContext)

    return (
        <>
        <Navbar/>
            {user && <div>
                <img src={user.image} alt="current-user" />
                <h3>{user.firstname}</h3>
                <p>{user.username}</p>
                <hr />

            </div>}
        </>
    )
}

export default User