const Guitar = ({guitar:{manufacturer, id, model, image, bodyType, strings}}) => {

    return(
        <>
            <h3>{manufacturer} - {model}</h3>
            <img src={image} alt={id} width={200} />
            <p>{bodyType} ({strings} strings)</p>
        </>
    )
}

export default Guitar