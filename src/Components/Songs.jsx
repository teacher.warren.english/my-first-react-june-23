function Songs({mySong, myRating, theGenre}) {
    
    // Destructuring syntax
    // const { firstname } = person
    
    return (
        <div>
            { mySong && <p>Song: { mySong }</p> }
            { myRating && <p>Rating: { myRating }</p> }
            { theGenre && <p>Genre: { theGenre }</p> }
            <hr />
        </div>
    )
}

export default Songs