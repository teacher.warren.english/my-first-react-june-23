import { useContext, useEffect, useState } from 'react'
import Auth from './HOC/Auth.jsx'
import Songs from './Components/Songs.jsx'
import { API_URL, USER_API_URL } from './const.js'
import Guitar from './Components/Guitars.jsx'
import { UserContext } from './Context/UserContext.jsx'
import Navbar from './Components/Navbar.jsx'

function App() {
  // VARIABLES
  const favSongs = [
    {
      title: "Kirby",
      rating: 5,
      genre: "Hip-Hop"
    },
    {
      title: "Boneyards",
      rating: 3.5,
      genre: "Metal"
    },
    {
      title: "Thank U, Next",
      rating: 4,
      genre: "Pop"
    }
  ]

  const songComponents = favSongs.map(song => <Songs mySong={song.title} myRating={song.rating} theGenre={song.genre} />)
  //let isShown = true

  //let guitars = []

  // HOOKS
  const [isShown, setIsShown] = useState(true)
  const [guitars, setGuitars] = useState([])

  const [user, setUser] = useContext(UserContext)

  useEffect(() => {
    console.log("Hello from the useEffect ⚡⚡⚡");
    fetch(API_URL)
      .then(resp => resp.json())
      .then(json => {
        console.log(json)
        setGuitars(json)
      })
      .catch(error => console.error(error.message))
  }, []) // [] = run once!

  useEffect(() => {
    fetch(USER_API_URL)
      .then(resp => resp.json())
      .then(json => {
        
        const newUser = {
          image: json.results[0].picture.medium,
          firstname: json.results[0].name.first,
          username: json.results[0].login.username
        }
        
        console.log(newUser)
        setUser(newUser)
      })
      .catch(error => console.error(error.message))
  }, []) // [] = run once!

  // FUNCTIONS
  function handleChange() {
    setIsShown(!isShown) //toggle
    console.log(isShown);
  }

  const renderGuitars = () => {
    return guitars.map(g => <Guitar guitar={g} />)
  }

  return (
    <>
    <Navbar/>
      { isShown && songComponents }
      <button onClick={ handleChange }>Show / Hide</button>
      { renderGuitars() }
    </>
  );
}

export default Auth(App);
