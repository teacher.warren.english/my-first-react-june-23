# Introduction to React | June 2023

This is a demonstration React project that uses:
1. React Components
2. React Hooks: useState(), useEffect
3. Higher Order Components

## Contributing

Warren West

## License

© Noroff Accelerate AS